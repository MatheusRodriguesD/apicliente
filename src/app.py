from flask import Flask, request
from flask_httpauth import HTTPBasicAuth
from flask_mysqldb import MySQL
from flask_restful import Resource, Api
import requests
from src.dao import ClienteDao, FavoritoDao
from src.models import Cliente as Cli, Favorito as prod

auth = HTTPBasicAuth()
app = Flask(__name__)
app.config.from_pyfile('config.py')
api = Api(app)
db = MySQL(app)
cliente_dao = ClienteDao(db)
produto_dao = FavoritoDao(db)

@auth.verify_password
def verificacao(login, senha):
    if not (login, senha):
        return False
    elif login == "Matheus" and senha == "1234":
        return True

class CadastraCliente(Resource):
    @auth.login_required
    def post(self):
        response = ''
        try:
            dados = request.json
            print(dados)
            cliente = Cli(email=dados['email'], nome=dados['nome'])
            retorno = cliente_dao.listar(cliente.email)
            if not retorno:
                if cliente_dao.salvar(cliente):
                    response = {
                        "mensagem": f"Cliente {cliente.nome} cadastrado com sucesso."
                    }
            else:
                response = {
                    "mensagem":f"Cliente {cliente.nome} já cadastrado no sistema."
                }
        except Exception:
            response = {
                "mensagem": f"Erro ao cadastrar o Cliente"
            }
        finally:
            return response

    @auth.login_required
    def put(self):
        response = ''
        try:
            dados = request.json
            cliente = Cli(email=dados['email'], nome=dados['nome'])
            retorno = cliente_dao.listar(cliente.email)
            if retorno:
                if cliente_dao.atualiza(cliente):
                    response = {
                        "mensagem": f"Cliente {cliente.nome} Atualizado com sucesso."
                    }
            else:
                response = {
                    "mensagem": f"Operação Invalida pois cliente não tem cadastro."
                }
        except Exception:
            response = {
                "mensagem": f"Erro ao atualizar o Cliente"
            }
        finally:
            return response

    @auth.login_required
    def delete(self):
        response = ''
        try:
            dados = request.json
            cliente_dao.deletar(dados["email"])
            response = {
                "mensagem": f"Cliente: {dados['email']} deletado com sucesso"
            }
        except Exception:
            response = {
                "mensagem": f"Erro ao deletar o Cliente"
            }
        finally:
            return response

class ListaCliente(Resource):
    @auth.login_required
    def get(self, email):
        response = ''
        try:
            cliente = cliente_dao.listar(email)
            if cliente:
                print(cliente)
                response = {
                    "nome": f"{cliente.nome}",
                    "email": f"{cliente.email}"
                }
            else:
                response = {
                    "mensagem": f"Cliente {email} não encontrado."
                }
        except Exception:
            response = {
                "mensagem": f"Erro ao Exibir o Cliente"
            }
        finally:
            return response

class CadastraProdutos(Resource):
    @auth.login_required
    def post(self):
        response = ''
        try:
            dados = request.json

            link = 'http://challenge-api.luizalabs.com/api/product/' + dados['idproduto']
            retorno = requests.get(link).json()
            produto = prod(price=retorno["price"],
                           image=retorno["image"],
                           brand=retorno["brand"],
                           idproduto=dados['idproduto'],
                           title=retorno["title"],
                           cliente=dados['email'])
            if retorno:
                achou = produto_dao.listar(produto)
                if not achou:
                    if produto_dao.salvar(produto):
                        response = {
                            "mensagem": f"Produto {produto.idproduto} cadastrado com sucesso para o cliente {produto.cliente}."
                        }
                else:
                    response = {
                        "mensagem": f"Produto {produto.idproduto} já cadastrado nos favoritos para esse cliente."
                    }
            else:
                response = {
                    "mensagem": f"Produto {produto.idproduto} não encontrado."
                }
        except Exception:
            response = {
                "mensagem": f"Erro ao adicionar na lista de favoritos"
            }
        finally:
            return response

    @auth.login_required
    def delete(self):
        response = ''
        try:
            dados = request.json
            produto_dao.deletar(dados['idproduto'], dados['email'])
            response = {
                "mensagem": f"Produto: {dados['idproduto']} deletado da lista de favoritos com sucesso"
            }
        except Exception:
            response = {
                "mensagem": f"Erro ao deletar produto da lista de favoritos"
            }
        finally:
            return response

class ListaProdutos(Resource):
    @auth.login_required
    def get(self, email):
        response = ''
        try:
            retorno = []
            prod = produto_dao.listar_tudo(email)
            if prod:
                for ret in prod:
                    retorno.append(ret)
                response = {
                    "mensagem": retorno
                }
            else:
                response = {
                    "mensagem": f"Não encontrado nenhum produto favorito para o Cliente {email}."
                }
        except Exception:
            response = {
                "mensagem": f"Erro ao Exibir o Cliente"
            }
        finally:
            return response

api.add_resource(CadastraCliente, '/Cliente/')
api.add_resource(ListaCliente, '/ListaCliente/<string:email>/')
api.add_resource(CadastraProdutos, '/ListaProduto/')
api.add_resource(ListaProdutos, '/ListaFavoritos/<string:email>/')

if __name__ == '__main__':
    app.run(debug=True)
