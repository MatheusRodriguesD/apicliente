from src.models import Cliente

SQL_DELETA_CLIENTE = 'DELETE from clientes where email = %s'
SQL_INSERE_CLIENTE = 'INSERT INTO clientes (email, nome) values (%s, %s)'
SQL_ATUALIZA_CLIENTE = 'UPDATE clientes set nome = %s, email = %s where email = %s'
SQL_BUSCA_CLIENTE = 'SELECT nome, email from clientes where email = %s'

SQL_DELETA_PRODUTO = 'DELETE from listaprodutos where id = %s and cliente = %s'
SQL_INSERE_PRODUTO = 'INSERT INTO listaprodutos ( price, image, brand, id, title, cliente) values (%s, %s, %s, %s, %s, %s)'
SQL_BUSCA_PRODUTO = 'SELECT * from listaprodutos where id = %s and cliente = %s'
SQL_LISTA_PRODUTO = 'SELECT price, image, brand, id as idproduto, title, cliente from listaprodutos where cliente = %s'

class ClienteDao:
    def __init__(self, db):
        self.__db = db

    def salvar(self, cliente):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_INSERE_CLIENTE, (cliente.email, cliente.nome))
        self.__db.connection.commit()
        return cliente

    def listar(self, email):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_BUSCA_CLIENTE, [email])
        tupla = cursor.fetchone()
        if tupla:
            return Cliente(tupla[0], tupla[1])
        else:
            return False

    def listar_tudo(self, email):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_BUSCA_CLIENTE, [email])
        tupla = cursor.fetchall()
        if tupla:
            return tupla
        else:
            return False

    def atualiza(self, cliente):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_ATUALIZA_CLIENTE, (cliente.nome, cliente.email, cliente.email))
        self.__db.connection.commit()
        return cliente

    def deletar(self, email):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_DELETA_CLIENTE, [email])
        retorno = self.__db.connection.commit()
        return retorno

class FavoritoDao:
    def __init__(self, db):
        self.__db = db

    def salvar(self, produto):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_INSERE_PRODUTO, (produto.price, produto.image, produto.brand, produto.idproduto, produto.title, produto.cliente))
        self.__db.connection.commit()
        return produto

    def listar(self, produto):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_BUSCA_PRODUTO, [produto.idproduto, produto.cliente])
        tupla = cursor.fetchone()
        if tupla:
            return produto
        else:
            return False

    def listar_tudo(self, cliente):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_LISTA_PRODUTO, [cliente])
        tupla = cursor.fetchall()
        if tupla:
            return tupla
        else:
            return False

    def deletar(self, idproduto, cliente):
        cursor = self.__db.connection.cursor()
        cursor.execute(SQL_DELETA_PRODUTO, [idproduto, cliente])
        retorno = self.__db.connection.commit()
        return retorno
