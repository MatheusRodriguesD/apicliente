create database ListaFavoritos;

create table clientes (
	email varchar(250) not null unique,
    nome varchar(150) not null
);

create table listaprodutos (
	price varchar(250),
    image varchar(250),
    brand varchar(100),
    id varchar(250),
    title varchar(100),
    cliente varchar(250)
);