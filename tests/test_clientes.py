from src.models import Cliente, Favorito as Prod
import requests

def test_deve_instanciar_cliente_quando_for_requisitado():
    cliente = Cliente(nome='Matheus', email="Matheus.rodrigues@consorcioluiza.com.br")
    assert cliente.nome == 'Matheus' and cliente.email == "Matheus.rodrigues@consorcioluiza.com.br"

def test_deve_instanciar_lista_de_produto_quando_for_requisitado():
    produto = Prod(price='79.9',
                   image="http://challenge-api.luizalabs.com/images/7f45bf16-812b-3ec0-03f7-6c2dd10bd95c.jpg",
                   brand="real techniques",
                   idproduto="7f45bf16-812b-3ec0-03f7-6c2dd10bd95c",
                   title="Pincel Sculpting Blush Brush",
                   cliente="Teste@teste.com.br")
    assert produto.idproduto == "7f45bf16-812b-3ec0-03f7-6c2dd10bd95c" and produto.cliente == "Teste@teste.com.br"

def test_deve_retornar_uma_mensagem_de_sucesso_quando_o_cliente_for_inserido_no_banco():
    link = 'http://127.0.0.1:5000/Cliente/'
    retorno = requests.post(link, json={
	    "nome": "Teste",
	    "email": "Teste@teste.com.br"
    }, auth=('Matheus', '1234'))
    retorno = retorno.json()
    assert retorno["mensagem"] == f"Cliente Teste cadastrado com sucesso."


def test_deve_retornar_erro_quando_cliente_ja_estiver_cadastrado():
    link = 'http://127.0.0.1:5000/Cliente/'
    retorno = requests.post(link, json={
        "nome": "Teste",
	    "email": "Teste@teste.com.br"
    }, auth=('Matheus', '1234'))
    retorno = retorno.json()
    assert retorno["mensagem"] == f"Cliente Teste já cadastrado no sistema."


def test_deve_retornar_uma_mensagem_quando_atualizado_com_sucesso():
    link = 'http://127.0.0.1:5000/Cliente/'
    retorno = requests.put(link, json={
        "nome": "Teste Testado",
        "email": "Teste@teste.com.br"
    }, auth=('Matheus', '1234'))
    retorno = retorno.json()
    assert retorno["mensagem"] == f"Cliente Teste Testado Atualizado com sucesso."

def test_deve_retornar_uma_mensagem_de_sucesso_quando_o_produto_for_inserido_no_banco():
    link = 'http://127.0.0.1:5000/ListaProduto/'
    retorno = requests.post(link, json={
        "idproduto": "7f45bf16-812b-3ec0-03f7-6c2dd10bd95c",
        "email": "Teste@teste.com.br"
    }, auth=('Matheus', '1234'))
    retorno = retorno.json()
    assert retorno["mensagem"] == f"Produto 7f45bf16-812b-3ec0-03f7-6c2dd10bd95c cadastrado com sucesso para o cliente Teste@teste.com.br."

def test_deve_retornar_erro_quando_produto_ja_estiver_cadastrado():
    link = 'http://127.0.0.1:5000/ListaProduto/'
    retorno = requests.post(link, json={
        "idproduto": "7f45bf16-812b-3ec0-03f7-6c2dd10bd95c",
        "email": "Teste@teste.com.br"
    }, auth=('Matheus', '1234'))
    retorno = retorno.json()
    assert retorno["mensagem"] == f"Produto 7f45bf16-812b-3ec0-03f7-6c2dd10bd95c já cadastrado nos favoritos para esse cliente."

def test_deve_retornar_sucesso_quando_os_clientes_forem_listados():
    link = 'http://127.0.0.1:5000/ListaCliente/Teste@teste.com.br'
    retorno = requests.get(link, auth=('Matheus', '1234'))
    assert retorno.status_code == 200

def test_deve_retornar_erro_quando_nao_existir_clientes_para_serem_forem_listados():
    link = 'http://127.0.0.1:5000/ListaCliente/TesteNaoTem@teste.com.br'
    retorno = requests.get(link, auth=('Matheus', '1234'))
    retorno = retorno.json()
    assert retorno["mensagem"] == f"Cliente TesteNaoTem@teste.com.br não encontrado."

def test_deve_retornar_sucesso_quando_clientes_para_serem_listados():
    link = 'http://127.0.0.1:5000/ListaFavoritos/Teste@teste.com.br'
    retorno = requests.get(link, auth=('Matheus', '1234'))
    assert retorno.status_code == 200

def test_deve_retornar_erro_quando_nao_existir_produtos_para_serem_listados():
    link = 'http://127.0.0.1:5000/ListaFavoritos/TesteNãoTem@teste.com.br'
    retorno = requests.get(link, auth=('Matheus', '1234'))
    retorno = retorno.json()
    assert retorno["mensagem"] == f"Não encontrado nenhum produto favorito para o Cliente TesteNãoTem@teste.com.br."

def test_deve_retornar_uma_mensagem_quando_o_produto_for_deletado_com_sucesso():
    link = 'http://127.0.0.1:5000/ListaProduto/'
    retorno = requests.delete(link, json={
        "idproduto": "7f45bf16-812b-3ec0-03f7-6c2dd10bd95c",
        "email": "Teste@teste.com.br"
    }, auth=('Matheus', '1234'))
    retorno = retorno.json()
    assert retorno["mensagem"] == f"Produto: 7f45bf16-812b-3ec0-03f7-6c2dd10bd95c deletado da lista de favoritos com sucesso"

def test_deve_retornar_uma_mensagem_quando_o_cliente_for_deletado_com_sucesso():
    link = 'http://127.0.0.1:5000/Cliente/'
    retorno = requests.delete(link, json={
        "email": "Teste@teste.com.br"
    }, auth=('Matheus', '1234'))
    retorno = retorno.json()
    assert retorno["mensagem"] == f"Cliente: Teste@teste.com.br deletado com sucesso"
