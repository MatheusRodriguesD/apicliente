## API CLIENTE - Documentação

Esta documentação descreve a estrutura da API de clientes.

<SERVIDOR> representa a uri - Ex: http://127.0.0.1:5000


## INICIAR
---

 1. Para iniciar o projeto é preciso criar o banco de dados MYSQL (arquivo se encontra no BitBucket com o nome de "ConfigInicial\bd.sql")
 2. depois instalar todas as bibliotecas necessarias para a execução da aplicação (arquivo se encontra no BitBucket com o nome de "ConfigInicial\requeriments.txt")


## Autenticação
---

 type: Basic

 login: Matheus

 senha: 1234

 obs: Todos os métodos requerem autenticação


## Clientes
---

    Estrutura de clientes

        Nome: Nome do cliente.

        Email: Email do cliente.


## Cadastra Cliente
---

 URL: <SERVIDOR>/cliente/


    Método POST

        Entrada: Json {'nome' : '', 'email': ''}

        Saida: Json{ 'mensagem' : 'mensagem'}


    Método PUT

        Entrada: Json {'nome' : '', 'email': ''}

        Saida: Json{ 'mensagem' : 'mensagem'}


    Método DELETE

        Entrada: Json {'email': ''}

        Saida: Json{ 'mensagem' : 'mensagem'}


## Lista cliente
---

 <EMAIL> representa o email do cliente que esta sendo requisitado.

    Método GET

        URL: <SERVIDOR>/ListaCliente/<EMAIL>

        Saida: Json{ 'nome' : '', 'email' : ''}


## Produtos
---

    Estrutura Produtos

        price: preço do produto

        image: URL da imagem do produto

        brand: marca do produto

        idproduto: id do produto

        title: nome do Produto

        cliente: email do cliente que solicitou o produto favorito


## Cadastra Produto Favorito
---

 URL: <SERVIDOR>/ListaProduto/


    Método POST

        Entrada: Json {'idproduto' : '', 'cliente': ''}

        Saida: Json{ 'mensagem' : 'mensagem'}


    Método DELETE

        Entrada: Json {'idproduto' : '', 'cliente': ''}

        Saida: Json{ 'mensagem' : 'mensagem'}

## Lista Produto Favorito
---

 <EMAIL> representa o email do cliente que esta sendo requisitado.

    Método GET

        URL: <SERVIDOR>/ListaFavoritos/<EMAIL>

        Saida: Json{ 'price' : '', 'image' : '', 'brand' : '', 'id' : '', 'title' : '', 'cliente' : '' }
